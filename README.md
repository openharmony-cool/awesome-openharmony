# Awesome OpenHarmony

## 介绍
OpenHarmony是由开放原子开源基金会（OpenAtom Foundation）孵化及运营的开源项目，目标是面向全场景、全连接、全智能时代，搭建一个智能终端设备操作系统的框架和平台，促进万物互联产业的繁荣发展。

本仓库汇集OpenHarmony开发的相关资源，欢迎您贡献资源及建议。

## 官方资源
* [OpenHarmony官网](https://www.openharmony.cn)
* [OpenHarmony源码仓库](https://gitee.com/openharmony)
